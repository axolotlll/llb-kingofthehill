﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BepInEx;
using GameplayEntities;
using UnityEngine;
using HarmonyLib;
using LLGUI;
using LLHandlers;
using LLScreen;
using Multiplayer;
using Debug = UnityEngine.Debug;

namespace BallHog
{
    [BepInPlugin("com.github.axolotl-code.ballhog", "Ball Hog", "1.0.0")]
    public class BallHogMod : BaseUnityPlugin
    {
        private const string modversion = "1.0.0";

        public static HpFactor infiniteHp;
        public static GameMode hogMode;
        public static string hogName;
        //public static LLButton hogButton;

        static BallHogMod()
        {
            infiniteHp = (HpFactor) 100;
            hogMode = (GameMode) 10;
            hogName = "koth";

            

            //hogButton = UnityEngine.Object.Instantiate<LLButton>()
            //hogButton.onClick = (LLClickable.ControlDelegate) (playerNr => BallHogMod.CreateHogLobby(playerNr));
        }

        public static void CreateHogLobby(int playerNr)
        {
            IJDANPONMLL.startGameMode = BallHogMod.hogMode;
            DNPFJHMAIBP.HOGJDNCMNFP(JOFJHDJHJGI.FADIBJAINDE);
        }

        void Awake()
        {
            Debug.Log("Patching ball hog mod....");

            var harmony = new Harmony("com.github.axolotl-code.ballhog");
            harmony.PatchAll();
        }
        
        
    }

    [HarmonyPatch(typeof(GetHitBallEntity), nameof(GetHitBallEntity.GetDamage))]
    class BallPatch
    {
        static bool Prefix(ref HHBCPNCDNDH __result)
        {
            HpFactor hp = JOMBNFKIHIC.GIGAKBJGFDI.BKKMIBGLAEC;
            if (hp == BallHogMod.infiniteHp)
            {
                //__result = HHBCPNCDNDH.DBOMOJGKIFI;
                __result = HHBCPNCDNDH.NKKIFJJEPOL(0);
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(World), nameof(World.Init1))]
    class WorldPatch
    {
        static void Postfix()
        {
            if (JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN == BallHogMod.hogMode)
            {
                World.instance.gameObject.AddComponent<BallHogScript>();
            }
            
        }
    }

    [HarmonyPatch(typeof(HDLIJDBFGKN), nameof(HDLIJDBFGKN.JMNACNEGEGL), new []{typeof(JOFJHDJHJGI), typeof(Message)})]
    class MenuPatch
    {
        static bool Prefix(JOFJHDJHJGI OHBPPCEFBHI, Message EIMJOIEPMNA)
        {
            HDLIJDBFGKN.IIACBHIDBKO iiacbhidbko = new HDLIJDBFGKN.IIACBHIDBKO();
            Msg msg = EIMJOIEPMNA.msg;
            if (msg == (Msg)114)
            {
                //if (iiacbhidbko.INJIBDOANLB != 0)
                //    return true;
                GameMode gameMode = HPNLMFHPHFD.ELPLKHOLJID.PNJOKAICMNN + 1;
                if (gameMode == GameMode._1v1)
                    gameMode = GameMode.FREE_FOR_ALL;
                while (!EPCDKLCABNC.KFFJOEAJLEH(gameMode) || gameMode == GameMode.FREE_FOR_ALL)
                {
                    gameMode += 1;
                    if (gameMode == GameMode.TEAM)
                        gameMode = BallHogMod.hogMode;
                    else if (gameMode == BallHogMod.hogMode)
                    {
                        gameMode = GameMode.FREE_FOR_ALL;
                    }
                }

                Debug.Log("Gamemode selected :" + gameMode);
                P2P.SendAll(new Message((Msg)137, 0, (int)gameMode));

                return false;
            }
            return true;
        }
    }

    // ensure ball hog is always unlocked
    [HarmonyPatch(typeof(EPCDKLCABNC), nameof(EPCDKLCABNC.KFFJOEAJLEH), new[]{typeof(GameMode)})]
    class GameModePatcher
    {
        static bool Prefix(GameMode PNJOKAICMNN, ref bool __result)
        {
            if (PNJOKAICMNN == BallHogMod.hogMode)
            {
                __result = true;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(JPLELOFJOOH), nameof(JPLELOFJOOH.EPBCINJEDAP), new []{typeof(GameMode)})]
    class MenuNamePatch
    {
        static bool Prefix(GameMode JMMBJPELKGG, ref string __result)
        {
            if (JMMBJPELKGG == BallHogMod.hogMode)
            {
                __result = BallHogMod.hogName;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(JOMBNFKIHIC), nameof(JOMBNFKIHIC.ADDBHIFLMEI))]
    class SettingsPatch
    {
        static bool Prefix(JOMBNFKIHIC __instance)
        {
            if (__instance.PNJOKAICMNN == BallHogMod.hogMode)
            {

               
                
                __instance.BNGBHJDEJNC = true;
                __instance.LDEAKMILLHE = false;

                __instance.MLEKMMGIFMF = 0;

                __instance.KOBEJOIALMO = true; // also use points
                __instance.FONKOHIGBLE = true; // use points
                __instance.NAKDBIFCJDI = true; // ball tagging?
                __instance.HGHFBMLJGEM = 99; // points
                __instance.GKJAGGKNFCO = 200; // time
                __instance.BLADHBMDPPK = false; // infinite time
                __instance.HKOFJKJDPGL = BallType.REGULAR; // ball type
                __instance.KHOEFMFHDMC = 8; // min speed
                __instance.IKGAIFLLLAG = 4; // energy
                __instance.BKKMIBGLAEC = BallHogMod.infiniteHp; // hp factor
                __instance.BFLJHKPDNKO = PowerupSelection.OFF; // powerup selection
                __instance.JFMNKHHLOOM = 0;

                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(ScreenPlayersSettings), nameof(ScreenPlayersSettings.GetSettingTexts))]
    class SettingsHpPatch
    {
        static void Postfix(out string hpFactor)
        {
            if (ScreenPlayersSettings.curGameSettings.BKKMIBGLAEC == BallHogMod.infiniteHp)
            {
                hpFactor = "Infinite";
            }
            else
            {
                HpFactor bkkmibglaec = ScreenPlayersSettings.curGameSettings.BKKMIBGLAEC;
                hpFactor = bkkmibglaec  != HpFactor.OFF ? JOMBNFKIHIC.NPBGJELKFGK(bkkmibglaec).ToString("0") + "%" : TextHandler.Get("TAG_OFF");
            }
        }
    }

    [HarmonyPatch(typeof(ScreenMenuVersus), nameof(ScreenMenuVersus.OnOpen))]
    class VersusMenuPatch
    {
        static void Postfix(ScreenMenuVersus __instance)
        {
            
            LLButton hogButton =
                UnityEngine.Object.Instantiate<LLButton>(__instance.btRoyale, __instance.btRoyale.transform);
            hogButton.transform.localPosition = __instance.btRoyale.transform.localPosition;

            hogButton.transform.localPosition = new Vector3(hogButton.transform.localPosition.x + 825, hogButton.transform.localPosition.y - 168, 0);
            hogButton.transform.localScale = __instance.btRoyale.transform.localScale * 1.44f;
            hogButton.name = "btHog";
            hogButton.SetText(BallHogMod.hogName);
            hogButton.onClick = new LLClickable.ControlDelegate(BallHogMod.CreateHogLobby);


        }
    }
    //
    [HarmonyPatch(typeof(NCMFHODLNAJ), nameof(NCMFHODLNAJ.HOGJDNCMNFP))]
    class GameRulesPatch
    {
        static bool Prefix(GameMode PNJOKAICMNN)
        {
            if (PNJOKAICMNN == BallHogMod.hogMode)
            {
                NCMFHODLNAJ.GIGAKBJGFDI = new PNGBDCNAECH();
                NCMFHODLNAJ.JHONJFDBPNB = NMNBAFMOBNA.CBPEHBCCEMG;
                return false;
            }

            return true;
        }
    }
    /*
    [HarmonyPatch(typeof(TextHandler), nameof(TextHandler.Get), new []{typeof(string), typeof(bool), typeof(string[])})]
    class TextHandlerPatch
    {
        static bool Prefix(string code,ref  string __result)
        {
            if (code == "KOTH")
            {
                __result = "KOTH";
                return false;
            }

            return true;
        }
    }
    */
    
    [HarmonyPatch(typeof(GameHudPlayerInfo), nameof(GameHudPlayerInfo.ShowPoints))]
    class HudPointsPatch
    {
        static bool Prefix(int points, GameHudPlayerInfo __instance)
        {
            if (JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN == BallHogMod.hogMode)
            {
                __instance.imtxtPointsCur.SetText(points.ToString());
                return false;
            }

            return true;
        }
    }
    
    
    [HarmonyPatch(typeof(GameHudPlayerInfo), nameof(GameHudPlayerInfo.Awake))]
    class HudHealthPatch
    {
        static void Postfix(GameHudPlayerInfo __instance)
        {
            if (JOMBNFKIHIC.GIGAKBJGFDI.BKKMIBGLAEC == BallHogMod.infiniteHp)
            {
                __instance.obHp.SetActive(false);
                __instance.imHpShown = false;
            }
        }
    }
    
}
