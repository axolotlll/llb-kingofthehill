﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameplayEntities;
using LLHandlers;
using LLScreen;
using UnityEngine;

namespace BallHog
{
    class BallHogScript : MonoBehaviour
    {
        //private EffectEntity crown;
        private bool running = false;
        private readonly float pointTimer = 0.6f;
        void Start()
        {
            JOMBNFKIHIC.GIGAKBJGFDI.BKKMIBGLAEC = BallHogMod.infiniteHp;
            InvokeRepeating("GivePoints", pointTimer, pointTimer);
            running = true;
            /*crown = EffectHandler.instance.GetSpareEffect();
        EffectData setData = new EffectData();
        setData.active = false;
        setData.frameSizePixels = new JKMAAHELEMF(32, 32);
        setData.animSpeed = HHBCPNCDNDH.NKKIFJJEPOL(0);
        setData.endAtAnimEnd = false;
        setData.duration = 2.5f;
        setData.graphicName = "crown";
        setData.parentPlayerIndex = World.NO_PLAYER_INDEX;
        setData.layer = Layer.INFRONT_GAMEPLAY;

        crown.ApplyEffectData(setData);*/
        }

        /*
         * ripped from effecthandler
         */
        void CreateCrownEffect(IBGCBLLKIHA pos, int playerIndex)
        {
            if (EffectHandler.noEffects)
                return;
            EffectEntity spareEffect = EffectHandler.instance.GetSpareEffect();
            EffectData setData = new EffectData();
            setData.active = true;
            setData.SetPositionData(IBGCBLLKIHA.GAFCIOAEGKD(pos, IBGCBLLKIHA.AJOCFFLIIIH(IBGCBLLKIHA.AJOCFFLIIIH(IBGCBLLKIHA.GGFFJDILCDA, HHBCPNCDNDH.NKKIFJJEPOL(160)), World.FPIXEL_SIZE)));
            setData.frameSizePixels = new JKMAAHELEMF(32, 32);
            setData.animSpeed = HHBCPNCDNDH.NKKIFJJEPOL(0);
            setData.endAtAnimEnd = false;
            setData.duration = pointTimer; // changed to last the points length
            setData.graphicName = "crown";
            setData.parentPlayerIndex = playerIndex;
            setData.layer = Layer.INFRONT_GAMEPLAY;
            spareEffect.ApplyEffectData(setData);
        }
        void GivePoints()
        {
            if (running)
            {
                //if (World.instance.ballHandler.balls.Length == 0) return;
                int index = World.instance.ballHandler.GetBall(0).ballData.lastHitterIndex;
                if (index != World.NO_PLAYER_INDEX)
                {
                    PlayerEntity ent = World.instance.playerHandler.GetPlayerEntity(index);
                    ent.playerData.points++;
                    ent.playerData.kills++;
                    CreateCrownEffect(IBGCBLLKIHA.DBOMOJGKIFI, index);
                    //EffectHandler.instance.CreatePlusPointEffect(ScreenGameHud.instance.playerInfos[index].portraitPos., true);
                    /*crown.effectData.parentPlayerIndex = index;
                    crown.effectData.active = true;
                    crown.effectData.SetPositionData(ent.playerData.position);
                    crown.ApplyEffectData(crown.effectData);*/
                    if (World.instance.playerHandler.GetPlayerEntity(index).playerData.points >= JOMBNFKIHIC.GIGAKBJGFDI.HGHFBMLJGEM)
                    {
                        EffectHandler.instance.CreateCrownEffect(IBGCBLLKIHA.DBOMOJGKIFI, index); // long lasting crown effect
                        running = false;
                        World.instance.ballHandler.SetAllBallDead();
                        World.SetTimeRunning(false);
                        
                        //ent.playerData.kills = 1;
                        NCMFHODLNAJ.GIGAKBJGFDI.NPNPJAGHINC(MKIGJKIBGIH: false);
                    }
                }
            }
        }

        void Update()
        {
            if (World.instance.ballHandler.balls.Length == 0) return;
            int index = World.instance.ballHandler.GetBall(0).ballData.lastHitterIndex;

            
            for (int i = 0; i < PlayerHandler.AmountPlayersIngame(); i++)
            {
                if (index == i)
                {
                    World.instance.playerHandler.GetPlayerEntity(i).SetColorOutlinesColor(Color.white);
                }
                World.instance.playerHandler.GetPlayerEntity(i).SetColorOutlines(index == i);

                
            }
        }
    }
}
